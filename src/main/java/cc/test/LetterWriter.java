/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.test;

import java.io.IOException;
import java.io.OutputStream;

/**
Class that writes fixed content to a given stream.

@author Will Provost
*/
public class LetterWriter
{
    public static final byte[] ALPHABET =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ".getBytes ();

    /**
    Writes a series of letters and spaces to the given stream.
    This version uses more than one overload of the write() method.
    */
    public static void writeAFewLetters (OutputStream stream)
        throws IOException
    {
        stream.write (97);
        stream.write (98);
        stream.write (99);
        stream.write (' ');
        stream.write (120);
        stream.write (121);
        stream.write (122);

        stream.close ();
    }

    /**
    Writes a series of letters and spaces to the given stream.
    This version uses more than one overload of the write() method.
    */
    public static void writeALotOfLetters (OutputStream stream)
        throws IOException
    {
        stream.write (97);
        stream.write (98);
        stream.write (99);
        stream.write (' ');
        stream.write (ALPHABET);
        stream.write (' ');
        stream.write (ALPHABET, 10, 10);
        stream.write (' ');
        stream.write (120);
        stream.write (121);
        stream.write (122);

        stream.close ();
    }
}
